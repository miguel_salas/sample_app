require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: {
                                name: "",
                                email: "name@invalid",
                                password:              "foo",
                                password_confirmation: "bar"
                              }
    end
    assert_template 'users/new'
  #  an other way to assert equal:
  #  before_count = User.count
  #  after_count  = User.count
  #  assert_equal before_count, after_count
  end
  
  test "valid signup information" do
    get signup_path
    assert_difference 'User.count', 1 do
      post_via_redirect users_path, user: {
                                            name: "Example User",
                                            email: "name@example.com",
                                            password: "foobar",
                                            password_confirmation: "foobar"
                                          }
    end
    # to force the post to follow the reditect
    #follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end

end
 