require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "ejemplo", email: "ejemplo@ejemplo.net",
                     password: "foobarbas", password_confirmation: "foobarbas")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "email should not be too long" do
    @user.email = "a" * 256
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@EXAMPLE.COM 
                        a_user-a@example.bar.com user@example.jp
                        alice+bob@they.cn]
    valid_addresses.each do |valid_address|
      @user.email =  valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end  
    
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_example.com
                          user@example. user@a_example.com 
                          user@alice+bob.com]
    invalid_addresses.each do |invalid_address|
      @user.email =  invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be valid"
    end
  end
  
  test "email should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = duplicate_user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password =  @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    # this user doesn't has a remember digest, 
    # and the remember token is irrelevant here
    assert_not  @user.authenticated?('')
  end
  
end
