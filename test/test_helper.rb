ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  
  fixtures :all
  
  # return true if a test user is logged in
  def is_logged_in?
    !session[:user_id].nil?
  end
  
  # Logs a test user.
  def log_in_as(user, options = {}) #takes an option  hash.
    password    = options[:password]    || 'password'
    remember_me = options[:remember_me] || '1'
    if integration_test?
      post login_path, session: {
                          email: user.email,
                          password: password,
                          remember_me: remember_me
      }
    else
      session[:user_id] = user.id
    end  
  end
  
  private
  
  def integration_test?
    # post_via_redirect is only defined inside an integration test.
    defined?(post_via_redirect)
  end
  
end
