module SessionsHelper
  # logs in a given user
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # Remembers a user in a persistent session
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] =  { value: user.id }
  # cookies.permanent is the same as puttin an expiration date of 20 years from
  # now:
  #                      ,  expires: 20.years.from_now.utc }
  # signed create an incripted singned cookie
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  # Forgets a persistent session
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
    
    
  end
  
  #retrieve the current logged-in in user
  def current_user
    if session[:user_id]
      # the rails way to do the thing, using the shorthand operator ||= ,
      # if @curent_user is nil,  it will be assigned with,
      # User.find_by(id: session[:user_id])
      @current_user ||= User.find_by(id: session[:user_id])
    elsif  cookies.signed[:user_id] # this is how to pull out a sign cookie. 
      user = User.find_by(id: cookies.signed[:user_id])
      if (user && user.authenticated?(cookies[:remember_token]))
        log_in user
        @current_user = user
      end
    end
  end
  
  def logged_in?
    !current_user.nil?
  end
  # logs out the current user
  def log_out
    forget(current_user)
    session.delete(:user_id)
    #this is the same from above:
    #session[:user_id] = nil
    @current_user = nil
  end
  
  # Confirms if the current user is the same as the given user
  def current_user?(user)
    user == current_user
  end
  
  # Redirect back to stored location or to the default.
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
  
  
  # Stores the location URL trying to be accesed.
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
  
end
