class User < ActiveRecord::Base
 attr_accessor :remember_token
  
  before_save {self.email =  self.email.downcase}
  validates :name,  presence: true, length: { maximum: 50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255},
            format: {with: VALID_EMAIL_REGEX},
            uniqueness: {case_sensitive: false} 
            #this implicitely has uniqueness true
  has_secure_password
  validates :password, length: { minimum: 6},
  allow_blank: true 
  # allow_blank  will not allow the creation of new users without password
  #because has secure password support a password presece validation
  # Returns the hash digest for a given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : 
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
    # Returns a random token
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Remembers a user in the database to for use  in persistent sessions. 
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
    # inside the model is unnecessary to refer the object, 
    # for example self.update_attribute
  end
  
  # Returns true if the given token matches the digest
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
  # Forgets a user, the digest will be set to nil.
  def forget
    update_attribute(:remember_digest, nil)
  end
end
