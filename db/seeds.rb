User.create!(name:  "Example User",
               email: "example@railstutorial.org",
               password:              "foobar",
               password_confirmation: "foobar",
               admin: true)
               
99.times do |number| 
  name  = Faker::Name.name
  email = "example-#{number+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end  